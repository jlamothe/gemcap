{-|

Module      : Network.Gemini.Capsule.Types
Description : Gemini capsule types
Copyright   : (C) Jonathan Lamothe
License     : AGPL-3.0-or-later
Maintainer  : jonathan@jlamothe.net
Stability   : experimental
Portability : POSIX

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public
License along with this program.  If not, see
<https://www.gnu.org/licenses/>.

-}

module Network.Gemini.Capsule.Types (
  -- * Types
  GemURL (..),
  GemRequest (..),
  GemResponse (..),
  GemHandler,
  GemCapSettings (..),
  -- * Constructors
  newGemURL,
  newGemRequest,
  newGemResponse,
  newGemCapSettings
) where

import qualified Data.ByteString.Lazy as BSL
import Data.Word (Word8, Word16, Word32)
import Data.X509 (Certificate)

-- | Gemini URL
data GemURL = GemURL
  { gemHost :: String
  -- ^ The host part of the authority section, e.g.: "example.com"
  , gemPort :: Maybe Word32
  -- ^ The port number (if supplied)
  , gemPath :: [String]
  -- ^ The decoded path segments
  , gemQuery :: Maybe String
  -- ^ The decoded request query (if supplied)
  } deriving (Eq, Show)

-- | Describes a Gemini request
data GemRequest = GemRequest
  { reqURL :: GemURL
  -- ^ The URL being requested
  , reqCert :: Maybe Certificate
  -- ^ The client certificate (if available)
  } deriving (Eq, Show)

-- | Describes a response to a Gemini request
data GemResponse = GemResponse
  { respStatus :: Word8
  -- ^ The response status code
  , respMeta :: String
  -- ^ The response metadata
  , respBody :: Maybe BSL.ByteString
  -- ^ The response body
  } deriving (Eq, Show)

-- | Handles a 'GemRequest' to produce a 'GemResponse'
type GemHandler = GemRequest -> IO GemResponse

-- | The settings required to set up a Gemini capsule
data GemCapSettings = GemCapSettings
  { capConnections :: Int
  -- ^ Number of simultaneous connections allowed
  , capPort :: Word16
  -- ^ The capsule port number
  , capCert :: FilePath
  -- ^ The path to the TLS certificate
  , capCertChain :: [FilePath]
  -- ^ The paths to the chain certificates
  , capKey :: FilePath
  -- ^ The path to the private key
  } deriving (Eq, Show)

-- | Builds a new 'GemURL'
newGemURL
  :: String
  -- ^ The hostname
  -> GemURL
newGemURL host = GemURL
  { gemHost  = host
  , gemPort  = Nothing
  , gemPath  = []
  , gemQuery = Nothing
  }

-- | Builds a 'GemRequest'
newGemRequest
  :: GemURL
  -- ^ The request URL
  -> GemRequest
newGemRequest url = GemRequest
  { reqURL  = url
  , reqCert = Nothing
  }

-- | Builds a 'GemResponse'
newGemResponse :: GemResponse
newGemResponse = GemResponse
  { respStatus = 20
  , respMeta   = "text/gemini"
  , respBody   = Nothing
  }

-- | Builds a reasonable set of server settings.
newGemCapSettings
  :: FilePath
  -- ^ Path to the server certificate
  -> FilePath
  -- ^ Path to the private key
  -> GemCapSettings
newGemCapSettings cert key = GemCapSettings
  { capConnections = 100
  , capPort        = 1965
  , capCert        = cert
  , capCertChain   = []
  , capKey         = key
  }

--jl
