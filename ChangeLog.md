# Changelog for gemcap

## 0.1.0.1

- updated underlying libraries to more current versions
- fixed a bug that would cause the server to crash when the client aborted the handshake by rejecting the key
